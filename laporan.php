<?php
    include 'koneksi.php';
    require_once('dompdf/autoload.inc.php');
    use Dompdf\Dompdf;
    $dompdf = new dompdf();
//------------------------------------------------------------------------------------------

    $html = '<h2>Laporan Nilai Siswa</h2>';
    $html .= '<table border ="1" cellpadding="2" cellspacing="1" width="100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Nama Siswa</th>
                                <th>Alamat</th>
                                <th>Nilai Siswa</th>
                                <th>Grade</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>';
                     $html .= '<body>';
                     $query = 'SELECT * FROM nilai_siswa';
                     $mysql = mysqli_query($connect, $query);
                     while($row = mysqli_fetch_array($mysql)){
                         $html .= '<tr>';
                         $html .= '<td>'.$row["id"].'</td>';
                         $html .= '<td>'.$row["nama_siswa"].'</td>';
                         $html .= '<td>'.$row["alamat"].'</td>';
                         $html .= '<td>'.$row["nilai_siswa"].'</td>';
                         $html .= '<td>'.$row["grade"].'</td>';
                         $html .= '<td>'.$row["keterangan"].'</td>';
                         $html .= '</tr>';
                     }
                     $html .= '</body>';
                    $html .= '</table>';
//-------------------------------------------------------------------------------------------
    $dompdf->loadHtml($html);
    $dompdf->setPaper('A4','potrait');
    $dompdf->render();
    $dompdf->stream("" ,array("Attachment" => false) );
    exit;
?>