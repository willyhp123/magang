 <?php
include('header.php');
if($_SESSION['id']){
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Nilai Siswa</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Nilai Siswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
    
           <button type="button" class="btn btn-primary"
                                data-toggle="modal" data-target="#modal-default">
                            Tambah Data
                        </button>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
            <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Default Modal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action ="">
                    <div class="modal-body">
                            <div class ="form-group mb-2">
                                <label>Nama Siswa</label>
                                <input type ="text" name="nama_siswa"
                                       class ="form-control"
                                       placeholder="Nama Siswa">
                                <!---------name  "username" artinya text itu bernama username------->
                            </div>
                            <div class ="form-group mb-2">
                                <label>Alamat</label>
                                <input type ="text"
                                       name ="alamat" placeholder="Alamat" class ="form-control">
                                <!---------name = "email" artinya text itu bernama email------->
                            </div>
                            <div class ="form-group  mb-2">
                                <label>Nilai Siswa</label>
                                <input type ="text"
                                       name ="nilai_siswa"
                                       placeholder="Nilai Siswa"
                                       class ="form-control">
                              
                            </div>
                             <div class ="form-group  mb-2">
                                <label>Gambar</label>
                                <input type ="file"
                                       name ="gambar"
                                       placeholder="Nilai Siswa"
                                  >
                              
                            </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name ="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
      <table id="example1" class="table table-bordered table-striped">
            <thead>
            <th>No</th>
            <th>Nama Siswa</th>
            <th>Alamat</th>
            <th>Nilai Siswa</th>
            <th>Grade</th>
            <th>Keterangan</th>
            <th>Gambar</th>
            <th>Aksi</th>
            </thead>
            <tbody>
            <!----------------tampilan table----------------->
            <!----------------tampilkan database------------->
            <?php
            $query ="select * from nilai_siswa  ";
            $mysql = mysqli_query($connect,$query);

            while ($row = mysqli_fetch_array($mysql)){
                ?>
                <tr>
                    <td><?php echo $row['id']?></td>
                    <td><?php echo $row['nama_siswa']?></td>
                    <td><?php echo $row['alamat']?></td>
                    <td><?php echo $row['nilai_siswa']?></td>
                    <td><?php echo $row['grade']?></td>
                    <td><?php echo $row['keterangan']?></td>
                    <td><img src="gambar/<?php echo $row['gambar'] ?>" width="50px" height = "50px"></td>
                    <td>
                        <a href="edit.php?id=<?php echo $row['id'] ?>"
                           class ="btn btn-warning btn-sm">Edit</a>
                        <a href="delete.php?id=<?php echo $row['id'] ?>"
                           class ="btn btn-danger btn-sm">Delete</a>
                        <a href="laporan_nilai_siswa.php?id=<?php echo $row['id'] ?>"
                           class ="btn btn-primary btn-sm">Laporan</a>
                    </td>
                </tr>
                <?php
            }
            //-------------------------------tampilkan database------------------------//
            ?>
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php
include('footer.php');
}else{
    header('location:login.php');
}

  ?>
